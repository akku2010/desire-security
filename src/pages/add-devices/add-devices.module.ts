import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDevicesPage } from './add-devices';
import { SMS } from '@ionic-native/sms';
// import { NativePageTransitions } from '@ionic-native/native-page-transitions';
import { TranslateModule } from '@ngx-translate/core';
import { OnCreate } from './dummy-directive';



@NgModule({
  declarations: [
    AddDevicesPage,
    OnCreate
  ],
  imports: [
    IonicPageModule.forChild(AddDevicesPage),
    TranslateModule.forChild()
  ],
  exports: [
    OnCreate
  ],
  providers: [
    SMS,
  ]
})
export class AddDevicesPageModule {}
