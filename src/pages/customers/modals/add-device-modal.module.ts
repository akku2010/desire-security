import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddDeviceModalPage } from './add-device-modal';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    AddDeviceModalPage
  ],
  imports: [
    IonicPageModule.forChild(AddDeviceModalPage),
    TranslateModule.forChild()
  ]
})
export class AddDeviceModalPageModule {}
